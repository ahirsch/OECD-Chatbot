#  @ Copyright Inria, Ecole Polytechnique
#  Shared under the MIT license https://opensource.org/licenses/mit-license.php

# This file contains the functions used to create the metadata dictionaries for the SDMX tables supported
# A topic dictionnary contains all the information to compute the topic proximity score between a query and each table
# A dimension dictionnary contains the information to fill the dimensions of the table with the query

### IMPORT

# Python libraries import
import nltk
from nltk.corpus import stopwords
import pickle
import pandas as pd
import os
import time
import urllib
from urllib.request import urlopen, Request
# Utils import
import utils
from metadata_extraction import *
from utils import lower_list, is_word


# Header information for request
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


## Keywords extraction for the topic score

stops = set(stopwords.words('english'))
avoid_dim = ['REPORTING_COUNTRY', 'REF_AREA', 'TIME_PERIOD', 'COUNTERPART_COUNTRY']

# This function takes as input the url of the metadata of one table
# and returns the keywords extracted from the metadata, in 4 different categories
# (Title, Description, Dimension, Category)
# There is not so much to understand here, we just go look at the keywords in the different parts of the metadata,
# keep only the relevant ones (not stopwords)
# and convert it to the right format (using codelists for example)

def keyword(url):
    # loading the xml structure as a tree
    print(url)
    req = urllib.request.Request(url, headers=hdr)
    file = urlopen(req)
    tree = ET.parse(file)
    root = tree.getroot()
    header = root[0]
    structure = root[1]
    # getting the different parts of the metadata
    concepts = get_concepts(structure)
    info = get_info(structure)
    cat = get_category(structure)
    dim = get_dimensions(structure)

    cl = get_codelists(structure)
    cons = get_constaints(structure)


    # The 4 categories
    Name = []
    Description = []
    Dimensions = []
    Category = []


    # Name keywords
    try :
        n = info["Name"]
        for w in nltk.word_tokenize(n):
            if (w.lower() not in stops and is_word(w.lower())):
                Name.append(w.lower())
    except :
        pass

    # Description keywords
    try :
        d = info["Description"]
        for w in nltk.word_tokenize(d):
            if (w.lower() not in stops and is_word(w.lower())):
                Description.append(w.lower())
    except :
        pass

    # Category keywords
    for c in cat:
        for w in nltk.word_tokenize(c):
            if (w.lower() not in stops and is_word(w.lower())):
                Category.append(w.lower())

    # Dimension keywords
    for a in dim:
        conc = concepts[dim[a][1]] #concept associated
        if (a not in avoid_dim):
            for w in nltk.word_tokenize(conc):      #words from the title of the dimension
                if (w.lower() not in stops and is_word(w.lower())):
                    Dimensions.append(w.lower())
            if a in cons:
                for c in cons[a]:                       #words from the values of the dimension
                    for d in cl[dim[a][2]]:
                        if d[0] == c:
                            for w in nltk.word_tokenize(d[1]):
                                if (w.lower() not in stops and is_word(w.lower())):
                                    Dimensions.append(w.lower())

    return (set(Name),set(Description),set(Dimensions),set(Category))

# Pretty print function for the 4 categories of keywords
def kprint(keyword):
    print("* Name : ", keyword[0])
    print("* Description : ", keyword[1])
    print("* Dimensions : ", keyword[2])
    print("* Category : ", keyword[3])
    print()


# Function to find the keywords for each supported table
# and save it as an independant file (topic_dict.pkl)
def topic_dict(path, name_list, url_list):
    d = {}
    n = len(name_list)
    for i in range(n):
        try:
            d[name_list[i]] = keyword(url_list[i])
        except urllib.error.HTTPError as u:
            print(u)
            print("topic_dict did not work:", name_list[i], url_list[i])
        if i%10 == 0:
            print(i*100/n)
    file = open(path, "wb")
    pickle.dump(d, file)
    file.close()


## Dimensions extraction for the dimension fill

# This function takes as input the url of the metadata of one table
# and for each of its dimension, and for each value of the dimension,
# gets the words in the name of the value and store them
# These keywords are then used in dimension_fill to find the most relevant value of one given dimension

def dimension(url):

    # loading the xml structure as a tree
    req = urllib.request.Request(url, headers=hdr)
    file = urlopen(req)
    tree = ET.parse(file)
    root = tree.getroot()
    header = root[0]
    structure = root[1]

    # getting the different parts of the metadata
    concepts = get_concepts(structure)
    info = get_info(structure)
    dim = get_dimensions(structure)

    cl = get_codelists(structure)
    cons = get_constaints(structure)

    # For some dimension, the OECD have specified a default value (for the default display of the table)
    # This can be found in the "information" part of the metadata (see metadata_extraction)
    # So first we try to build a default dictionnary with that
    info_def = {}
    try :
        defo = info["Default"]
        for s in defo:
            t = s.split("=")
            info_def[t[0]] = t[1]
    except:
        pass

    Dimensions = {} #Words for each value for each dimension
    Default = {}    #Default value (if any) for each dimension


    for a in dim:
        if (a not in avoid_dim):    #some dimensions (like the time) are voluntarily avoided
            values = []

        #First we try to find a default value

            # If there is only one value for the dimension, it is the default
            if a not in cons:
                print(url)
                print(cons)
            if len(cons[a]) == 1:
                for d in cl[dim[a][2]]:
                    if d[0] == cons[a][0]:
                        Default[a] = d
            # If one value has the code '_T' (for total), it is the default
            elif ('_T' in cons[a]):
                for d in cl[dim[a][2]]:
                    if d[0] == '_T':
                        Default[a] = d
            # If the word "total" appear in the name of one value, it is the default
            else:
                def_val = None
                for c in cons[a]:
                    for d in cl[dim[a][2]]:
                        if d[0] == c:
                            if 'total' in d[1].lower():
                                def_val = d
                Default[a] = def_val

            # Else, the dimension has no default value
            if Default[a] == None:
                try :
                    for d in cl[dim[a][2]]:
                        if d[0] == info_def[a]:
                            Default[a] = d
                except:
                    pass

        #Then, for each value, we its text and add these words to the dictionnary

            for c in cons[a]:
                for d in cl[dim[a][2]]:
                    if d[0] == c:
                        values.append(d)

            Dimensions[a] = values
    return (Dimensions,Default)

# This function executes the previous one to all the supported tables
# and save it as an independant file (dim_dict.pkl)

def dim_dict(path, name_list, url_list):
    d = {}
    n = len(name_list)
    for i in range(n):
        try:
            d[name_list[i]] = dimension(url_list[i])
        except urllib.error.HTTPError as u:
            print(u)
            print("dim_dict did not work:", name_list[i], url_list[i])
    file = open(path, "wb")
    pickle.dump(d, file)
    file.close()




### Endpoint analysis

# Extract endpoint information
path = os.getcwd()
endpoints = os.path.join(path,"data/endpoints.csv")
df_endpoints = pd.read_csv(endpoints, sep = ';')
endpoints_dict = {row["Name"]: (row["Site"],row["Rest"], "dataflow/") for _,row in df_endpoints.iterrows()}

# Get all important info for dataflow
def get_all_info(dataflow):
    Category = None
    Code = "/".join([dataflow.attrib["agencyID"], dataflow.attrib["id"], dataflow.attrib["version"]])
    Description = None
    for a in dataflow:
        if (get_tag(a) == 'Name' and list(a.attrib.values())[0] == 'en'):
            Name = a.text
        if (get_tag(a) == 'Description' and list(a.attrib.values())[0] == 'en'):
            Description = a.text
    return (Name, Name, Category, Code)


# Get all dataflow information and store it in data/tables
available_endpoints = []
for endpoint in endpoints_dict:
    print(endpoint)
    url_name = "".join(endpoints_dict[endpoint])
    print(url_name)
    path = "data/tables/" + endpoint + ".csv"

    if os.path.exists(path):
        available_endpoints.append(endpoint)
        print("EXISTS")
    else:
        try:
            req = urllib.request.Request(url_name, headers=hdr)
            file = urlopen(req)
            tree = ET.parse(file)
            header, structure = tree.getroot()

            data = [get_all_info(dataflow) for dataflow in structure[0]]
            df = pd.DataFrame(data, columns = ["Full Name", "Short Name", "Category", "Code"])

            df.to_csv(index = False, path_or_buf = path, sep = ";")

            available_endpoints.append(endpoint)
            print("DONE")
        except (urllib.error.HTTPError,urllib.error.URLError) as u:
            print(u)


### PATHS

path = os.getcwd()
tables_path = os.path.join(path,"data/tables")
topics_path = os.path.join(path,"data/topics")
dims_path = os.path.join(path,"data/dims")

#df_path = os.path.join(path, "data/tables/DEMO.csv")
#topic_path = os.path.join(path, "data/topic_dict.pkl")
#dim_path = os.path.join(path, "data/dim_dict.pkl")

### LOADINGS

region_dict = utils.region_dict
country_list = lower_list(region_dict["World"])

structure_suffix = "?references=all&detail=referencepartial"


def extract_topic_dict(endpoint):
    t1 = time.time()
    file_csv = endpoint + ".csv"
    file_pkl = endpoint + ".pkl"
    df_path = os.path.join(tables_path, file_csv)
    topic_path = os.path.join(topics_path, file_pkl)
    dim_path = os.path.join(dims_path, file_pkl)

    structure_prefix = "".join(endpoints_dict[endpoint])

    df_tables = pd.read_csv(df_path, sep = ";")


    url_list = []   #list of the url (one per table)
    name_list = []  #list of the table names

    print(df_tables)

    for id, row in df_tables.iterrows():
        name = row["Short Name"]
        code = row["Code"]
        full_code = structure_prefix + code + structure_suffix

        name_list.append(name)
        url_list.append(full_code)

    topic_dict(topic_path, name_list, url_list)
    dim_dict(dim_path, name_list, url_list)
    t2 = time.time()
    print("Time taken for ", endpoint,":",t2-t1)

'''
for endpoint in available_endpoints:
    extract_topic_dict(endpoint)
'''
extract_topic_dict("Pacific Community")
