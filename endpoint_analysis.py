### Endpoint analysis
import os
import pandas as pd
from urllib.request import urlopen
from metadata_extraction import *
import xml.etree.ElementTree as ET

path = os.getcwd()
endpoints = os.path.join(path,"data/endpoints.csv")
endpoints_dict = {}

df_endpoints = pd.read_csv(endpoints, sep = ';')
for _, row in df_endpoints.iterrows():
    endpoints_dict[row["Name"]] = [row["Site"],row["Dataflow"]]
#print(endpoints_dict)


for x in endpoints_dict:
    id = {}
    print(x)
    url_name = "".join(endpoints_dict[x])
    print(url_name)
    file = urlopen(url_name)
    tree = ET.parse(file)
    header, structure = tree.getroot()
    for dataflow in structure[0]:
        print((dataflow[1].text,dataflow[1].text))
        #id[dataflow.attrib["id"]] = dataflow[1].text
    print(id)
    print()
